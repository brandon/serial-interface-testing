import serial
import time
# [FIVEBITS, SIXBITS, SEVENBITS, EIGHTBITS], [PARITY_NONE, PARITY_EVEN, PARITY_ODD PARITY_MARK, PARITY_SPACE]
# [STOPBITS_ONE, STOPBITS_ONE_POINT_FIVE, STOPBITS_TWO]'
# Bauds: 50, 75, 110, 134, 150, 200, 300, 600, 1200, 1800, 2400, 4800, 9600, 19200, 38400, 57600, 115200. These are well supported on all platforms.
# Standard values above 115200, such as: 230400, 460800, 500000, 576000, 921600, 1000000, 1152000, 1500000, 2000000, 2500000, 3000000, 3500000, 4000000 also work on many platforms and devices.
#s = serial.Serial('/dev/ttyACM0', 9600, timeout=.1)
s = serial.Serial(port='/dev/ttyACM0', baudrate=9600, bytesize=EIGHTBITS, parity=PARITY_EVEN, stopbits=STOPBITS_ONE, timeout=.1)
wait=5
time.sleep(1)
print(s.read(100))

s.write(b'64,0,0\n')
print(s.read(100))
time.sleep(wait)

s.write(b'0,128,0\n')
print(s.read(100))
time.sleep(wait)

s.write(b'0,0,5\n')
print(s.read(100))
time.sleep(wait)

s.close()
exit()
